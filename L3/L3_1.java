import java.util.Scanner;


public class L3_1 
{
	public static void main(String[] args) 
	{
		int lewy=-1, prawy=-1;
		int p=0, np=0, ujem=0, zer=0, dod=0, max, maxq=0, sujem=0, sdod=0, najdl=0,najdltmp=0;

		Scanner in = new Scanner(System.in);
		int n = -1;
		while(n<1 || n>100)
		{
		n = in.nextInt();
		if(n<1 || n>100) System.out.println("Wczytaj ponownie");
		}

		int[] arr = new int[n];
		for(int i = 0; i < n; i++)
		{
		arr[i] = (int)(Math.random()*2000)-1000;
		System.out.print(arr[i] + " ");
		}
		System.out.println();
		max = arr[0];
		

		for(int i = 0; i < n; i++)
		{
			if(arr[i]%2 == 0) p++;
			if(arr[i]<0)
			{
				ujem++;
				sujem+=arr[i];
			}
			if(arr[i]>0)
			{
				dod++;
				sdod+=arr[i];
			}
			if(arr[i]>max) max = arr[i];
		}

		for(int i = 0; i < n; i++)
		{
			if(arr[i]==max) maxq++;
			if(arr[i]>0) najdltmp++;
			else
			{
				if(najdltmp>najdl) najdl=najdltmp;
				najdltmp=0;
			}
		}
		np = n-p;
		zer = n-ujem-dod;
		System.out.println("a) Parzystych:" + p);
		System.out.println("a) Nieparzystych:" + np);
		System.out.println("b) Ujemnych:" + ujem);
		System.out.println("b) Zerowych:" + zer);
		System.out.println("b) Dodatnich:" + dod);
		System.out.println("c) Najwiekszy:" + max);
		System.out.println("c) Najwiekszy ilosc:" + maxq);
		System.out.println("d) Suma dod:" + sdod);
		System.out.println("d) Suma ujem:" + sujem);
		System.out.println("e) Najdluzszy dod lancuch:" + najdl);
		System.out.print("f) ");
		for(int i = 0; i<n; i++)
		{
			if(arr[i]>0) arr[i]=1;
			if(arr[i]<0) arr[i]=-1;
			System.out.print(arr[i] + " ");
		}
		System.out.println();

		while(lewy<1 || lewy>n || prawy<1 || prawy>n)
		{
			lewy = in.nextInt();
			prawy = in.nextInt();
			lewy-=1;
			prawy-=1;
			if(lewy<1 || lewy>n || prawy<1 || prawy>n) System.out.println("Ponownie!");
		}
		System.out.print("g) ");

		for(int i = prawy; i>=lewy; i--)
			System.out.print(arr[i]+" ");
	}
}