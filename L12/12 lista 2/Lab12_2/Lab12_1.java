import java.util.*;
public class Lab12_1
{
    public static void main(String[] args)
    {
        LinkedList<Integer> b = new LinkedList<>();
        b.add(1);
        b.add(2);
        b.add(3);
        b.add(4);
        b.add(5);
        b.add(6);
        redukuj(b, 2);
        System.out.println(b);
    }
    public static void redukuj(LinkedList<?> pracownicy, int n)
    {
        Iterator<?> bIter = pracownicy.listIterator();
        while (bIter.hasNext()) 
        {
            boolean tst = true;
            for(int i = 0; i<n; i++)
            {
                if (bIter.hasNext()) bIter.next();
                else tst = false;
            }
            if(tst) bIter.remove();
        }
    }
}
