import java.util.Scanner;
import java.util.Collections;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
public class Lab12_4
{
    public static void main(String[] args)
    {
        if (args.length != 1) {
            System.err.println("Sposób użycia: java TestFileScanner nazwaPliku");
            System.exit(1);
        }
        Scanner file;
        Map<Integer, HashSet<String>> Hashes = new TreeMap<Integer, HashSet<String>>();
        try {
            file = new Scanner(new File(args[0]));
            while (file.hasNext()) {
                String line = file.next();
                int h = line.hashCode();
                HashSet<String> Kostyl = new HashSet<String>();
                if(Hashes.get(h)!=null) Kostyl = Hashes.get(h);
                Kostyl.add(line);
                HashSet<String> A = new HashSet<String>(Kostyl);
                Hashes.put(h, A);
                //Hashes.put(h,new HashSet<String>(Hashes.get(h).add(line)));
            }
        } catch (FileNotFoundException e) {
            System.err.println("FileNotFoundException: " + e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            System.err.println("Caught Exception: " + e.getMessage());
            System.exit(2);
        }
        finally{
                for (Map.Entry<Integer, HashSet<String>> wpis : Hashes.entrySet()) {
            int key = wpis.getKey();
            HashSet value = wpis.getValue();
            if(value.size()>1)System.out.println("klucz: " + key + ", wartość: " + value);
        }        
        }
        
    }
}

