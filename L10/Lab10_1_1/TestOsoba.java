import pl.imiujd.trots.*;

import java.util.*;
import java.time.LocalDate;
import java.util.ArrayList;
public class TestOsoba
{
    public static void main(String[] args)
    {
        ArrayList<Osoba> grupa = new ArrayList<Osoba>();
        grupa.add(new Osoba("Kate", LocalDate.of(2001, 12, 06)));
        grupa.add(new Osoba("Nata", LocalDate.of(2000, 05, 29)));
        grupa.add(new Osoba("Diana", LocalDate.of(2000, 05, 26)));
        grupa.add(new Osoba("Sasha", LocalDate.of(1999, 11, 26)));
        grupa.add(new Osoba("Kary", LocalDate.of(2000, 01, 06)));
        for (Osoba p : grupa) {
            System.out.println(p);
        }
        Collections.sort(grupa);
        System.out.println("After sort");
        for (Osoba p : grupa) {
            System.out.println(p);
        }
    }
}

