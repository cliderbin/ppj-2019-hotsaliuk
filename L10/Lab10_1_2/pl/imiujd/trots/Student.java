package pl.imiujd.trots;
import java.time.LocalDate;
public class Student extends Osoba
{    
    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen)
    {
        super(nazwisko,dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }
    public int compareTo(Student other)
    {
        int supercmp = super.compareTo(other);
        if(supercmp!=0) return supercmp;
        double supercmpr = sredniaOcen - other.sredniaOcen;
        if(supercmpr!=0) return (int)(supercmp/Math.abs(supercmp));
        return (int)supercmp;
    }
    private double sredniaOcen;
}
