import java.util.*;
import java.text.SimpleDateFormat;

public class Lab11_3
{
    public static void main(String[] args)
    {
        Integer[] a = {5,5,6,2,1,23,4,6,7,8};
        for(int i =0 ; i<a.length; i++)
        System.out.println(a[i]);
        System.out.println("Is Sorted: "+ArrayUtil.isSorted(a));
        ArrayUtil.selectionSort(a);
        for(int i =0 ; i<a.length; i++)
        System.out.println(a[i]);
        System.out.println("Is Sorted: "+ArrayUtil.isSorted(a));
        System.out.println("Find 7, index = "+ArrayUtil.binSearch(a,7));
    }
}

class ArrayUtil
{
    public static <T extends Comparable<T>> boolean isSorted(T[] a)
    {
        boolean is = true;
        for(int i = 0; i<a.length-2; i++)
        {
            if(a[i].compareTo(a[i+1])==1) is = false;
        }
        return is;
    }
    public static <T extends Comparable<T>> int binSearch(T[] a, T Sz)
    {
        int index = -1;
        int low = 0;
        int high = a.length - 1;
        while(low<=high)
        {
            int mid = (low + high)/2;
            if(a[mid].compareTo(Sz)==-1) low = mid+1;
            else if(a[mid].compareTo(Sz)==1) high = mid-1;
            else if(a[mid].compareTo(Sz)==0)
            {
                index = mid;
                break;
            }
        }
        return index;
    }
    public static <T extends Comparable<T>> void selectionSort(T[] arr)
    {
        int n = arr.length;
        for(int i = 0; i<n-1; i++)
        {
            int min_idx = i;
            for(int j = i+1; j<n; j++)
                if(arr[j].compareTo(arr[min_idx])==-1)
                    min_idx = j;
            T temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }
    }

}



