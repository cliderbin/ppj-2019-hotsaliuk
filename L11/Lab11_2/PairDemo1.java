public class PairDemo1
{
    public static void main(String[] args)
    {
        String[] words = { "Ala", "ma", "psa", "i", "kota" };
        Pair<String> mm = new Pair<String>("First","Second");
        System.out.println("First = " + mm.getFirst());
        System.out.println("Second = " + mm.getSecond());
        Pair<String> mn = new Pair<String>();
        mn = PairUtil.swap(mm);
        System.out.println("First = " + mn.getFirst());
        System.out.println("Second = " + mn.getSecond());
    }
}

class PairUtil
{
    public static <T> Pair<T> swap(Pair<T> A)
    {
        return new Pair<T> (A.getSecond(), A.getFirst());
    }
}
