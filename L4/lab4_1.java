import java.util.Scanner;
import java.util.Random;

public class lab4_1
{
	
	static int countChar(String str, char c)
	{	
		int temp=0;
		for (int i=0; i<str.length(); i++)
		{
		if (str.charAt(i)==c) temp++;
		}
		return temp;
	}
	

	static int countSubStr(String str, String Substr)
	{	
		int temp=0;
		for (int i=0; i<str.length(); i++)
		{
		if (str.charAt(i)==Substr.charAt(0) && i+Substr.length()<=str.length()) 
			{	
			int r=0;
			for (int j=0; j<Substr.length(); j++)
				if(str.charAt(i+j)!=Substr.charAt(j)) r=1;
			if (r==0) temp++;
			}
			
		}
		return temp;
	}


    static int[] countSubStr22(String str, String Substr)
	{	
		int temp=0;
		for (int i=0; i<str.length(); i++)
		{
		if (str.charAt(i)==Substr.charAt(0) && i+Substr.length()<=str.length()) 
			{	
			int r=0;
			for (int j=0; j<Substr.length(); j++)
				if(str.charAt(i+j)!=Substr.charAt(j)) r=1;
			if (r==0) temp++;
			}
		}
        
        int arr[]=new int[temp];
        int k=0;
        for (int i=0; i<str.length(); i++)
		{
		if (str.charAt(i)==Substr.charAt(0) && i+Substr.length()<=str.length()) 
			{	
			int r=0;
			for (int j=0; j<Substr.length(); j++)
				if(str.charAt(i+j)!=Substr.charAt(j)) r=1;
			if (r==0) {arr[k]=i;  k++; }
			}
			
		}
		return arr;
	}

	static String middle(String str)
	{	String temp="";
		if (str.length()%2==0) temp+=String.valueOf(str.charAt(str.length()/2-1))+String.valueOf(str.charAt(str.length()/2));
		else	temp+=String.valueOf(str.charAt((str.length()+1)/2-1));
		return temp;
	}


	static String repeat(String str, int n)
	{	
		String temp="";
		for (int i=0; i<n; i++)
		temp+=str;
		return temp;
	}

    static String change(String str)
    {
        StringBuffer s = new StringBuffer();
        for(int i = 0; i<str.length(); i++)
        {
            Character c = str.charAt(i);
            if (Character.isLowerCase(c)) s.append(Character.toUpperCase(c));
            else if(Character.isUpperCase(c)) s.append(Character.toLowerCase(c));
            else s.append(c);
        }
        String a = s.toString();
        return a;

    }

    static String nice(String str)
    {
        StringBuffer s = new StringBuffer();
        int it = 1;
        for(int i = str.length()-1; i>=0; i--)
        {   
            if(it%4 == 0)
            {
                s.append("'");
                it=1;
            }
            Character c = str.charAt(i);
            s.append(c);
            it++;
        }
        return s.reverse().toString();
    }

    static String nice2(String str, char separ, int cnt)
    {
        StringBuffer s = new StringBuffer();
        int it = 1;
        for(int i = str.length()-1; i>=0; i--)
        {   
            if(it%(cnt+1) == 0)
            {
                s.append(separ);
                it=1;
            }
            Character c = str.charAt(i);
            s.append(c);
            it++;
        }
        return s.reverse().toString();
    }


	public static void main(String[] args) 
	{
	String str="Input information Input information Input information";
	char a='n';
	String b= "Input";
	int d=3;
    String Numb = "10020030040";
	
	System.out.println(str);
	System.out.println("a) '"+a+"'");
	System.out.println("amount of '"+a+"' = "+countChar(str, a));
	System.out.println("b) '"+b+"'");
	System.out.println("amount of '" + b + "' str = " + countSubStr(str, b));
	System.out.println("c) middle "+str.length());
	System.out.println("middle of  str = " + middle(str));
	System.out.println("d) "+d);
	System.out.println(repeat(str,d));
    int arr[] = countSubStr22(str, b);
    for(int i = 0; i<arr.length; i++)
    {
    System.out.println(arr[i]);
    System.out.println(change(b));
    System.out.println(nice(Numb));
    System.out.println(nice2(Numb, args[0].charAt(0), Integer.parseInt(args[1])));
    }
	}
}
