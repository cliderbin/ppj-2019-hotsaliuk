import pl.imiujd.trots.*;

import java.util.*;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Jan", "Kowalski", 50000, LocalDate.of(1990, 10, 20),LocalDate.of(1970, 10, 20),true );
        ludzie[1] = new Student("Małgorzata","Nowak","informatyka",LocalDate.of(1990, 10, 20),96.2,false);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()+p.getImie()+": " + p.getOpis()+"; plec: "+p.getPlec());
        }
    }
}

