package pl.imiujd.trots;
import java.time.LocalDate;
import java.util.*;
import java.text.*;
import java.time.format.DateTimeFormatter;
public abstract class Instrument
{
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
        
    }
    public abstract String dzwiek();
    public String getProducent()
    {
        return producent;
    }
    public LocalDate getRokProdukcji()
    {
        return rokProdukcji;
    }
    public String toString()
    {
        return getClass().getName()
               + "[producent=" + producent
               + ",rokProdukcji=" + rokProdukcji.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
               + "]";
    }
    public boolean equals(Object otherObject)
    {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (getClass() != otherObject.getClass()) {
            return false;
        }
        Instrument other = (Instrument) otherObject;
        return producent.equals(other.producent)
               && rokProdukcji.equals(other.rokProdukcji);
    }
    private String producent;
    private LocalDate rokProdukcji;
}
